import { reactive } from "vue";
import { useForm } from "@ant-design-vue/use";

export default function useAppService() {
  const model = reactive({
    user: {
      name: {
        enName: "",
        first: "",
        last: "",
        nested: "",
      },
      password: "",
    },
    token: "",
    type: 1,
  });

  const rules = reactive({
    token: [{ required: true, message: "不嵌套" }],
    "user.name.nested": [{ required: true, message: "嵌套" }],
    "user.name.first": [{ required: true, message: "中文姓" }],
    "user.name.last": [{ required: true, message: "中文名" }],
    "user.name.enName": [{ required: true, message: "英文名" }],
    "user.password": [{ required: true, message: "我需要你的银行卡密码" }],
  });

  const { validate, resetFields, validateInfos } = useForm(model, rules);

  return {
    model,
    rules,
    validate,
    resetFields,
    validateInfos,
  };
}
