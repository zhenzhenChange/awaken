import { inject } from "vue";

import { AwakenFormServiceToken } from "../AwakenForm/useAwakenFormService";
import { AwakenFormItemServiceToken } from "../AwakenFormItem/useAwakenFormItemService";

import usePathResolver from "../../composables/usePathResolver";

export default function useAwakenInputService() {
  const { name } = inject(AwakenFormItemServiceToken)!;
  const { model } = inject(AwakenFormServiceToken)!;

  if (/\./.test(name)) {
    const [finallyName, finallyModel] = usePathResolver(name, model);

    return { name: finallyName, model: finallyModel };
  }

  return { name, model };
}
