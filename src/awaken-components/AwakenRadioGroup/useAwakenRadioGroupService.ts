import { inject, InjectionKey } from "vue";
import { AwakenFormServiceToken } from "../AwakenForm/useAwakenFormService";
import { AwakenFormItemServiceToken } from "../AwakenFormItem/useAwakenFormItemService";

import usePathResolver from "../../composables/usePathResolver";

interface IRadio {
  text: string;
  value: string | number;
}

export const AwakenRadioGroupServiceToken: InjectionKey<{
  [key: string]: IRadio[];
}> = Symbol();

// 这里的逻辑与 Input 中的基本上相同，可以抽离复用
export default function useAwakenRadioGroupService() {
  const { name } = inject(AwakenFormItemServiceToken)!;
  const { model } = inject(AwakenFormServiceToken)!;

  const radios = inject(AwakenRadioGroupServiceToken)!;

  // 根据 name 字段，获取当前 radios
  const service = { name, model, radios: radios[name] };

  if (/\./.test(name)) {
    const [finallyName, finallyModel] = usePathResolver(name, model);

    service.name = finallyName;
    service.model = finallyModel;
  }

  return service;
}
