import type { InjectionKey } from "vue";

/**
 * @description 懒得写了
 */
export interface IAwakenForm {
  model: any;
  rules: any;
  validate: any;
  resetFields: any;
  validateInfos: any;
}

export const AwakenFormServiceToken: InjectionKey<IAwakenForm> = Symbol();
