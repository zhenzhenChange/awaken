import type { InjectionKey } from 'vue'

export const AwakenFormItemServiceToken: InjectionKey<{ name: string }> = Symbol()
