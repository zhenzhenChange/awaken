/**
 * @description 嵌套路径解析器
 *
 * @param key 嵌套的属性访问路径：a.b.c.d.e
 * @param target 目标对象
 * @returns 返回最后一个属性以及解包后剩下最后一个属性的对象 [e, { e: 'xx' }] | 以数组的形式返回方便命名
 */
export default function usePathResolver(key: string, target: any) {
  let i = 0

  const keys = key.split('.')

  while (i < keys.length - 1) {
    if (!target) break

    const currKey = keys[i++]

    if (currKey in target) target = target[currKey]
  }

  return [keys[i], target]
}
